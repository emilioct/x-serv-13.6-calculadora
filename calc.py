# Función para sumar
def sumar(n1, n2):
    print("Sumando", n1, "y", n2, "->", n1, "+", n2,  "=", n1+n2)

# Función para restar
def restar(n1, n2):
    print("Restando", n1, "a", n2, "->", n2, "-", n1,  "=", n2-n1)


# Programa principal
sumar(1, 2)
sumar(3, 4)
restar(5, 6)
restar(7, 8)
